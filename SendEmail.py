#!/usr/bin/env python

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def SendEmail(_to, _from, _subject, _htmlmsg, _plainmsg, _username, _password, _server="localhost", _port=25, _enctype="none"):
    COMMASPACE = ', '
    msg = MIMEMultipart('alternative')
    msg['From'] = _from
    if isinstance(_to, list):
        msg['To'] = COMMASPACE.join(_to)
    else:
        msg['To'] = _to
    msg['Subject'] = _subject
    # create the two types
    htmlpart = MIMEText(_htmlmsg, 'html')
    plainpart = MIMEText(_plainmsg, 'plain')
    # attach the parts
    msg.attach(plainpart)
    msg.attach(htmlpart)
    # create the mail server object
    mailserver = smtplib.SMTP(_server, _port)
    # identify ourselves to smtp gmail client
    mailserver.ehlo()
    # secure our email with tls encryption
    mailserver.starttls()
    # re-identify ourselves as an encrypted connection
    mailserver.ehlo()
    mailserver.login(_username, _password)
    mailserver.sendmail(_from, _to, msg.as_string())
    mailserver.quit()
