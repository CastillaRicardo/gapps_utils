#!/usr/bin/env python

import csv

def enter_data(_data_row, _file_path, _delimiter=',', _quotechar='\"', _quoting=csv.QUOTE_MINIMAL):
    try:
        with open(_file_path, 'ab+') as csvfile:
            # make sure that the file was ended with a CR+LF
            csvfile.seek(-1,2)
            if csvfile.read() == '\n':
                print "found end of line"
            else:
                print "no end of line"
                csvfile.write('\r\n')
            csvwriter = csv.writer(csvfile, delimiter=_delimiter,
                quotechar=_quotechar, quoting=_quoting)
            csvwriter.writerow(_data_row)
    except Exception as e:
        print "Excetion occurred: {}".format(e)
#
# def main():
#     row = ['Tester', 'McTest', 'goodstuff@example.com','','','','','','','','','','','','','']
#     path = '/Users/rcastilla/Documents/work/UserData-bio.fsu.csv'
#     enter_data(row, path)
#
# if __name__ == "__main__":
#     main()
